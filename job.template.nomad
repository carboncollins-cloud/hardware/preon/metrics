job "storage-metrics-preon" {
  name = "Storage Metrics (Preon)"
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "nas-preon"

  group "preon" {
    count = 1

    constraint {
      attribute = "${attr.cpu.arch}"
      value = "amd64"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "preon-metrics"
      task = "telegraf"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "telegraf" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 250
        memory = 300
      }

      config {
        image = "[[ .jobDockerImage ]]"

        cap_add = ["net_bind_service"]

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/preon" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_AUTH_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_ENCRYPTION_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "30s"
    healthy_deadline = "5m"
    progress_deadline = "10m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
